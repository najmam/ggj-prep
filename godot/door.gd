extends Node

export var door_name = "unnamed door"
signal door_entered(door_name)

func _on_door_entered(_body):
	if _body.is_in_group("player"):
		emit_signal("door_entered", door_name)
