extends Node2D

signal level_changed(idx, lvl)
signal finished_last_level

const tile_size = Vector2(16,16)
const _mob = preload("res://mob.tscn")
const _door = preload("res://door.tscn")

var cur_level = null
var cur_level_idx = 0

func spawn_mob(position, name):
	var mob = _mob.instance()
	mob.name = name
	mob.position = position
	$npcs.add_child(mob)

func spawn_door(position, name):
	var door = _door.instance()
	door.name = name
	door.door_name = name
	door.position = position
	$doors.add_child(door)
	door.connect("door_entered", self, "_on_door_entered")

func free_cur_level():
	cur_level.visible = false
	for npc in $npcs.get_children():
		npc.queue_free()
	for door in $doors.get_children():
		door.queue_free()

func load_level(idx):
	if cur_level != null:
		free_cur_level()
	cur_level_idx = idx
	cur_level = $levels.get_children()[cur_level_idx]
	cur_level.visible = true
	
	# fit the background to the level's bounding box
	$backg.rect_position = cur_level.position
	$backg.rect_size = cur_level.get_node("ground").get_used_rect().size * tile_size
	
	# place the player
	$player.position = cur_level.get_node("entities/player_start").position + tile_size/2 + cur_level.position
	# spawn mobs and doors
	for entity in cur_level.get_node("entities").get_children():
		if entity.name.find("npc") == 0:
			spawn_mob(entity.position + tile_size/2 + cur_level.position, entity.name)
		elif entity.name.find('door') == 0:
			spawn_door(entity.position + tile_size/2 + cur_level.position, entity.name)
	
	emit_signal("level_changed", cur_level_idx, cur_level)

func _on_door_entered(door_name):
	var next_level_idx = cur_level_idx + 1
	$sfx_door.play()
	if next_level_idx == $levels.get_child_count():
		emit_signal("finished_last_level")
	else:
		load_level(next_level_idx)

func _on_level_changed(idx, lvl):
	# show the level name for a brief duration
	var duration = 3
	$hud/level_name.text = "Level %d: %s" % [idx+1, lvl.get_meta("name")]
	$hud/level_name.visible = true
	$hud/level_name.modulate = Color.white
	yield(get_tree().create_timer(duration/2), "timeout")
	var tween = get_tree().create_tween()
	tween.tween_property($hud/level_name, "modulate", Color(255,255,255,0), duration/2)
	yield(get_tree().create_timer(duration/2), "timeout")
	$hud/level_name.visible = false

func _ready():
	load_level(0)
