extends Node

signal player_got_hurt(remaining_health)
signal player_died

const player_max_health = 6 # should be even for healthbar to work
var player_health = player_max_health

func _ready():
	pass

func hurt_player(amount):
	player_health -= amount
	player_health = max(0,player_health)
	if player_health > 0:
		emit_signal("player_got_hurt", player_health)
	else:
		emit_signal("player_died")

func quit():
	get_tree().notification(MainLoop.NOTIFICATION_WM_QUIT_REQUEST)
