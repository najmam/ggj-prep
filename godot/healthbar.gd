extends Node2D

const heart = preload("res://heart.tscn")
const nb_hearts = Global.player_max_health / 2
var instances = []
const tile_size = Vector2(16,16)
const position_topleft = tile_size*3/4
const position_step = Vector2(tile_size.x+5,0)

func _ready():
	for i in range(0,nb_hearts):
		var inst = heart.instance()
		inst.position = position_topleft + i*position_step
		instances.append(inst)
		self.add_child(inst)

func _process(delta):
	for i in range(0,nb_hearts):
		var inst = instances[i]
		inst.get_node("empty").visible = Global.player_health <= 2*i
		inst.get_node("half").visible = Global.player_health == 2*i + 1
		inst.get_node("full").visible = Global.player_health >= 2*i + 2
