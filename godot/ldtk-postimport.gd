extends Object

func post_import(map: Node2D):
	for level in map.get_children():
		(level as Node2D).visible = false
	return map
