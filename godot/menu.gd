extends Control

signal ui_quit

func _ready():
	$ItemList.grab_focus()
	$ItemList.select(0)

func _on_item_activated(index):
	match index:
		0: get_tree().change_scene("res://game.tscn")
		1: get_tree().change_scene("res://credits.tscn")
		2: emit_signal("ui_quit")
