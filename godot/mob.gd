extends KinematicBody2D

enum Behavior { RANDOM, PLAYER_VICINITY }
var behavior = Behavior.RANDOM
onready var destination = position

var destination_distance = 30
var speed_norm = 100
const epsilon = 10

func _ready():
	$timer_brownian_tick.wait_time = rand_range(0, 0.6)

func _on_timer_brownian_tick_timeout():
	var v = (Vector2.RIGHT * destination_distance).rotated(randf()*TAU)
	destination = position + v
	
	$timer_brownian_tick.wait_time = rand_range(0.7, 1.7)
	destination_distance = rand_range(25, 35)
	speed_norm = rand_range(90, 110)

func _process(_delta):
	# brownian motion
	if behavior == Behavior.RANDOM:
		var v = destination - position
		if v.length() > epsilon:
			v = v.normalized() * speed_norm
			var _coll = move_and_slide(v)
