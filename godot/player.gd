extends KinematicBody2D

export var speed_norm = 150
export var speed_running_multiplier = 1.8

var go_left = false
var go_right = false
var go_down = false
var go_up = false

var dx = 0
var dy = 0
var running = false
var attacking = false

var direction = Vector2.ZERO
var direction_when_hurt = Vector2.ZERO
var just_got_hurt = 0
const hurt_duration = 8 # nb of frames during which recoil happens
const hurt_recoil = 1.5

func _ready():
	pass

func _process(_delta):
	var is_dead = Global.player_health == 0
	
	## physics
	# walk, running and being hurt have different movement speeds
	var dir = direction_when_hurt if just_got_hurt > 0 else direction
	var v = dir \
		* speed_norm \
		* (speed_running_multiplier if running else 1) \
		* (hurt_recoil if just_got_hurt else 1.0)
	# when hurt, the player can't use controls for N frames
	just_got_hurt = max(0, just_got_hurt-1)
	# compute collisions
	if not is_dead:
		var _lin_velocity = self.move_and_slide(v)
		for i in get_slide_count():
			var collision = get_slide_collision(i)
			if collision.collider.is_in_group("enemy"):
				just_got_hurt = hurt_duration
				$sfx_hurt.play()
				direction_when_hurt = collision.normal
				Global.hurt_player(1)
				break # ignore consecutive slides
	
	# animation
	var state = "idle"
	if v.length() != 0 and not is_dead:
		$AnimationTree["parameters/idle/blend_position"] = v
		$AnimationTree["parameters/walk/blend_position"] = v
		$AnimationTree["parameters/attack/blend_position"] = v
		$AnimationTree["parameters/die/blend_position"] = v
		state = "walk"
	state = "attack" if attacking else state
	state = "die" if is_dead else state
	$AnimationTree["parameters/playback"].travel(state)
	
	# reset
	if attacking:
		attacking = false

func _input(event):
	direction = Input.get_vector("player_left", "player_right", "player_up", "player_down")	
	if event.is_action_pressed("player_run"):
		running = true
	elif event.is_action_released("player_run"):
		running = false
	elif event.is_action_pressed("player_attack"):
		attacking = true
		$sfx_attack.play()
