extends Node
	
func _on_quit():
	Global.quit()

func _input(event):
	if event.is_action_pressed("game_quit"):
		_on_quit()
